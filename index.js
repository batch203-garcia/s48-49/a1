// Get all data.
let fetchMovies = () => {
    fetch("https://sheltered-hamlet-48007.herokuapp.com/movies")
        .then(response => response.json())
        .then(data => showPosts(data));
}

fetchMovies();

// Show fetch data in the document (HTML web page)
const showPosts = (movies) => {

    console.log(movies);

    // Create a variable that will contain all the movies.
    let movieEntries = "";

    //forEach() to loop each movie in our movies object array.
    movies.forEach((movie) => {
        // onclick() is an event occurs when the user click on an element.
        // This allows us to execute a JavaScript's function when an element get clicked.

        // We can assign HTML elements in JS Variables.
        movieEntries += `
			<div id="movie-${movie._id}">
				<h3 id="movie-title-${movie._id}">${movie.title}</h3>
				<p id="movie-desc-${movie._id}">${movie.description}</p>
				<button onclick="editMovie('${movie._id}')">Edit</button>
				<button onclick="deleteMovie('${movie._id}')">Delete</button>
			</div>
		`;
    });

    //console.log(movieEntries);
    // To display the movies in our HTML document
    document.querySelector("#div-movie-entries").innerHTML = movieEntries;
}

// Add Movie data
// We select the Add form using the query selector
// We listen with the submit button for events.
document.querySelector("#form-add-movie").addEventListener("submit", (e) => {

    // Prevents the page from loading
    e.preventDefault();

    let title = document.querySelector("#txt-title").value
    let description = document.querySelector("#txt-desc").value

    fetch("https://sheltered-hamlet-48007.herokuapp.com/movies", {
            method: "POST",
            body: JSON.stringify({
                title: title,
                description: description
            }),
            headers: {
                "Content-Type": "application/json"
            }
        })
        .then(response => response.json())
        .then(data => {
            console.log(data);
            alert("Succesfully added.");

            //querySelector = null
            document.querySelector("#txt-title").value = null;
            document.querySelector("#txt-desc").value = null;

            fetchMovies();
        });
});

//Edit Movie
const editMovie = (id) => {
    console.log(id);

    let title = document.querySelector(`#movie-title-${id}`).innerHTML;
    let desc = document.querySelector(`#movie-desc-${id}`).innerHTML;

    console.log(title);
    console.log(desc);

    document.querySelector("#txt-edit-id").value = id;
    document.querySelector("#txt-edit-title").value = title;
    document.querySelector("#txt-edit-description").value = desc;
    document.querySelector("#btn-submit-update").removeAttribute("disabled");
}

//Update a movie

document.querySelector("#form-edit-movie").addEventListener("submit", (e) => {
    e.preventDefault();

    let id = document.querySelector("#txt-edit-id").value;

    fetch(`https://sheltered-hamlet-48007.herokuapp.com/movies/${id}`, {
            method: "PUT",
            body: JSON.stringify({
                title: document.querySelector("#txt-edit-title").value,
                description: document.querySelector("#txt-edit-description").value
            }),
            headers: {
                "Content-Type": "application/json"
            }
        })
        .then(response => response.json())
        .then(data => {
            console.log(data);
            alert("Successfully updated!")
            document.querySelector("#txt-edit-id").value = null;
            document.querySelector("#txt-edit-title").value = null;
            document.querySelector("#txt-edit-description").value = null;
            document.querySelector("#btn-submit-update").setAttribute("disabled", true);

            fetchMovies();
        })
})



//Delete a movie



const deleteMovie = (id) => {

    fetch(`https://sheltered-hamlet-48007.herokuapp.com/movies/${id}`, {
            method: "DELETE"
        })
        .then(response => response.json())
        .then(data => {
            console.log(data);
            alert("Successfully deleted!")

            fetchMovies();
        })
}